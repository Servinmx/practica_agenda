<?php


include "app/Models/Conexion.php";
include "app/Models/Contacto.php";

use Models\Conexion;
use Models\Contacto;
class ContactoController
{
    public function agregar(){
        if(!isset($_POST["nombre"]) && !isset($_POST["apellido"]) && !isset($_POST["telefono"]) && !isset($_POST["correo"])){
            echo "Error, faltan datos";
        }else{
            $contacto = new Contacto();
            $contacto->nombre = $_POST["nombre"];
            $contacto->apellido = $_POST["apellido"];
            $contacto->telefono = $_POST["telefono"];
            $contacto->correo = $_POST["correo"];
            $contacto->insert();
        }
    }

    public function editar (){
        if(isset($_POST)){
            $id = $_POST["id"];
            $contacto = new Contacto();
            $contacto->nombre = $_POST ["nombre"];
            $contacto->apellido = $_POST ["apellido"];
            $contacto->telefono = $_POST ["telefono"];
            $contacto->correo = $_POST ["correo"];
            $contacto-> id = $id;
            $contacto->update();
        }
    }

}