<?php


namespace Models;


class Contacto extends Conexion
{
    public $id;
    public $nombre;
    public $apellido;
    public $telefono;
    public $correo;

    function insert(){
        $pre = mysqli_prepare($this->con, "INSERT INTO contactos (nombre, apellido, telefono, correo) VALUES (?,?,?,?)");
        $pre->bind_param("ssss", $this->nombre, $this->apellido, $this->telefono, $this->correo);
        $pre->execute();
        if(!$pre){
            echo "Error al registrar usuario";
        }else{
            echo "Contacto Guardado Correctamente";
        }
    }

    function update (){
        $me = new Conexion();
        $pre = mysqli_prepare($me -> con, "UPDATE contactos SET nombre=?, apellido=?, telefono=?, correo=? WHERE id=?");
        $pre -> bind_param ("ssssi", $this -> nombre, $this -> apellido, $this -> telefono, $this -> correo, $this -> id);
        $pre -> execute();
        if(!$pre){
            echo "No se pudo editar";
        }
        else{
            echo "Editado correctamente";
        }
        return true;
    }
}